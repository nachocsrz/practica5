/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 * ArbolAVL.java Clase que genera arboles binarios de busqueda de tipo AVL, los
 * cuales cuentan con un metodo de insercion que depende del factor de
 * equilibrio de sus nodos, llevando a cabo rotaciones en caso de estar
 * desbalanceados
 *
 * @author Castillo Suarez Ignacio, Mendez Olivares Ana Itzel
 * @param <E> Tipo de dato generico del arbolAVL
 * @version Oct 2019
 */
public class ArbolAVL<E extends Comparable<E>> extends ArbolBinarioBusqueda<E> {

    /**
     * Metodo constructor que recibe un objeto de tipo NodoAVL para su raiz.
     *
     * @param n Nodo para inicializar la raiz del arbol
     */
    public ArbolAVL(NodoAVL<E> n) {
        super(n);
    }

    /**
     * Metodo que lleva a cabo la rotacion derecha del arbol a segun de su
     * factor de equilibrio. Si el arbol se desbalancea, el metodo entra y
     * realiza una rotacion con el objetivo de volver a equilibrar
     *
     * @param padre Nodo a balancear
     */
    public void rotacionDerecha(NodoAVL<E> padre) {

        //Creamos un nodo auxiliar con el elemento del nodo padre
        //Identificamos el nuevo padre, en este caso sería el hijo izquierdo del nodo padre
        NodoAVL<E> aux = new NodoAVL(padre.getElemento());
        NodoAVL<E> nuevoPadre = (NodoAVL) padre.getIzquierdo();

        //Si el nodo padre tiene hijo derecho, lo asignamos como hijo derecho
        //del nodo auxiliar
        if (padre.getDerecho() != null) {
            aux.setDerecho(padre.getDerecho());
        }

        //Si el nuevo padre tiene un hijo derecho, lo asignamos como hijo izquierdo
        //del nodo auxiliar
        if (nuevoPadre.getDerecho() != null) {
            NodoAVL<E> rot = (NodoAVL) nuevoPadre.getDerecho();
            aux.setIzquierdo(rot);
        }

        //Ahora el nodo con el elemento del nodo padre es el  hijo derecho del nuevo padre
        nuevoPadre.setDerecho(aux);
        //Asignamos los atributos del nuevo padre al nodo padre
        padre.setElemento(nuevoPadre.getElemento());
        padre.setIzquierdo(nuevoPadre.getIzquierdo());
        padre.setDerecho(nuevoPadre.getDerecho());

    }

    /**
     * Metodo que lleva a cabo la rotacion izquierda del arbol a segun de su
     * factor de equilibrio. Si el arbol se desbalancea, el metodo entra y
     * realiza una rotacion con el objetivo de volver a equilibrar
     *
     * @param padre Nodo a balancear
     */
    public void rotacionIzquierda(NodoAVL<E> padre) {
        //Creamos un nodo auxiliar con el elemento del nodo padre
        //Identificamos el nuevo padre, en este caso sería el hijo derecho
        NodoAVL<E> aux = new NodoAVL(padre.getElemento());
        NodoAVL<E> nuevoPadre = (NodoAVL) padre.getDerecho();

        //Si el nodo padre tiene hijo izquierdo, lo asignamos como hijo izquierdo
        //del nodo auxiliar
        if (padre.getIzquierdo() != null) {
            aux.setIzquierdo(padre.getIzquierdo());
        }

        //Si el nuevo padre tiene un hijo izquierdo, lo asignamos como hijo derecho
        //del nodo auxiliar
        if (nuevoPadre.getIzquierdo() != null) {
            NodoAVL<E> rot = (NodoAVL) nuevoPadre.getIzquierdo();
            aux.setDerecho(rot);
        }

        //Ahora el nodo con el elemento del nodo padre es hijo izquierdo del nuevo padre
        nuevoPadre.setIzquierdo(aux);
        //Asignamos los atributos del nuevo padre al nodo padre
        padre.setElemento(nuevoPadre.getElemento());
        padre.setIzquierdo(nuevoPadre.getIzquierdo());
        padre.setDerecho(nuevoPadre.getDerecho());

    }

    /**
     * Metodo auxiliar el cual inserta un nuevo nodo al arbol ya establecido.
     * Dependiendo del factor de equilibrio, el metodo realiza una rotacion
     * simple a la derecha o izquierda, o una rotacion doble a la derecha o
     * izquierda
     *
     * @param n Nodo del cual se va a desprender otro nodo
     * @param elemento Nodo que se desea añadir al arbol
     * @return Nodo ya modoficado, con sus rotaciones necesarias
     */
    private void insertar(E elemento, NodoAVL<E> n) {
        super.insertar(elemento, n);
    }

    /**
     * Metodo que realiza la insercion de elementos al arbol
     *
     * @param elemento Elemento que se desea añadir
     */

    @Override
    public void insertar(E elemento) {
        insertar(elemento, (NodoAVL<E>) raiz);
        equilibrarNodo((NodoAVL<E>) raiz);
    }

    /**
     * Metodo que rota el nodo en caso de que no esté equilibrado
     *
     * @param nodo Nodo a balancear dependiendo de su factor de equilibrio
     */
    public void equilibrarNodo(NodoAVL<E> nodo) {
        if (nodo.getDerecho() != null) {
            equilibrarNodo((NodoAVL) nodo.getDerecho());
        }
        if (nodo.getIzquierdo() != null) {
            equilibrarNodo((NodoAVL) nodo.getIzquierdo());
        }
        if (nodo.getFactor() < -1) {
            rotacionIzquierda(nodo);
        } else {
            if (nodo.getFactor() > 1) {

                rotacionDerecha(nodo);
            }
        }

    }

}
