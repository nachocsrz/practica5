/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 * NodoAVL.java
 * Clase que genera nodos de tipo AVL, los cuales cuentan con un factor de 
 * equilibrio, util para el posterior balanceo de los arbolesAVL.
 * @author Castillo Suarez Ignacio, Mendez Olivares Ana Itzel
 * @param <E> Tipo de dato generico del nodoAVL
 * @version Oct 2019
 */
public class NodoAVL <E> extends Nodo<E>{

    private int factorEquilibrio;
    /**
     * Constructor de NodoAVL por parametros, llamando al constructor de la
     * clase padre para su funcionamiento, este recibe un elemento de tipo E
     * y crea un nodo con dicho objeto
     * @param elemento Objeto contenido por el nodo
     */
    public NodoAVL(E elemento) {
        super(elemento);
        
    }
    /**
     * Constructor por parametros que recibe un elemento y los hijos izquierdo
     * y derecho para el nodo; usa el constructor de la clase padre para 
     * la creacion del objeto
     * @param izquierdo Hijo izquierdo del nodo
     * @param derecho Hijo derecho del nodo
     * @param elemento Objeto contenido por el nodo
     */
    public NodoAVL(NodoAVL<E> izquierdo, NodoAVL<E> derecho, E elemento){
        super(izquierdo, derecho, elemento);
    }
    /**
     * Metodo que regresa el factor de equilibrio de un nodoAVL determinado, 
     * el factor de equilibrio es usado para las rotaciones del arbol de 
     * ser necesarias.
     * @return El numero entero, calculado por medio de la resta de alturas
     * izquierda y derecha
     */
    public int getFactor() {
        calcularFactor();
        return factorEquilibrio;
    }
    /**
     * Metodo que establece un nuevo factor de equilibrio para el nodoAVL
     * @param nuevoFactor El nuevo factor del nodo
     */
    public void setFactor(int nuevoFactor) {
        nuevoFactor = factorEquilibrio;
    }
    
    /**
     * Metodo que calcula el factor de equilibrio del nodo AVL, para esto 
     * se apoya de la altura del nodo, determinando el factor haciendo la resta
     * entre la altura izquierda menos la altura derecha. 
     */
    public void calcularFactor() {
        //Si la altura es 0, el factor es 0
        if (getAltura() == 0) {
            factorEquilibrio = 0;
        } else {
            //Si no tiene hijo Izquierdo, el factor es la altura del nodo por -1
            if (getIzquierdo() == null) {
                factorEquilibrio = -getAltura();
            } else {
                //Si no tiene hijo derecho, el factor es la altura del nodo
                if (getDerecho() == null) {
                    factorEquilibrio = getAltura();
                } else {
                    //Si tiene ambos hijo, el factor es la altura del izquierdo
                    // menos la altura del derecho
                    int izq = getIzquierdo().calcularAltura();
                    int der = getDerecho().calcularAltura();
                    factorEquilibrio = izq - der;
                }
            }
        }
    }
    
    /**
     * Metodo que crea el hijo izquierdo o derecho para el nodo deseado, se 
     * apoya de un booleano para saber en qué posicion añadirlo
     * @param esIzquierdo Booleano para conocer el lado a añadir el hijo
     */
    @Override
    public void crearHijo(boolean esIzquierdo) {
        //En este metodo, el hijo que se crea es AVL, necesario para
        //usar el metodo de insertar en arbolAVL
        NodoAVL aux = new NodoAVL(null, null, null);
        if (esIzquierdo) {
                setIzquierdo(aux);
        } else {
                setDerecho(aux);

        }

    }
}
