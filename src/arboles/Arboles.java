/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 * Arboles.java
 * Clase que contiene al main; en esta clase se realizan la creacion de los 
 * arboles binario y AVL, asi como la insercion de sus elementos
 * @author Castillo Suarez Ignacio, Mendez Olivares Ana Itzel
 * @version Oct 2019
 */
public class Arboles {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //Creacion de la raiz y del arbol binario de busqueda con esta raiz
        Nodo<Integer> raiz = new Nodo<>(null);
        ArbolBinarioBusqueda arbolito = new ArbolBinarioBusqueda(raiz);
        
        //Insercion de los valores del 10 al -5 por medio de un ciclo for
        for (int i = 10; i > -6; i--) {
            arbolito.insertar(i);
        }
        //Representacion del arbol binario de busqueda
        System.out.println(arbolito);
        
        //Impresion de los recorridos del arbol binario de busqueda
        System.out.println("Recorrido Preorden Arbol Binario");
        arbolito.recorrerPreorden();
        System.out.println("\nRecorrido Postorden Arbol Binario");
        arbolito.recorrerPostorden();
        System.out.println("\nRecorrido Inorden Arbol Binario");
        arbolito.recorrerInorden();
       
       //Creacion de la raiz y del arbolAVL
        NodoAVL<Integer> raizAVL = new NodoAVL<>(null);
        ArbolAVL arbolAVL = new ArbolAVL(raizAVL);

       //Insercion de los elementos del arbolAVL por medio de un ciclo for
        for (int i = 10; i > -6; i--) {
            arbolAVL.insertar(i);
        }
       
        
        //Representacion del arbolAVL
        System.out.println("\n" + arbolAVL);
        
        //Impresion de los recorridos del arbolAVL
        System.out.println("\nRecorrido Preorden ArbolAVL");
        arbolAVL.recorrerPreorden();
        System.out.println("\nRecorrido PostordenArbolAVL");
        arbolAVL.recorrerPostorden();
        System.out.println("\nRecorrido InordenArbolAVL");
        arbolAVL.recorrerInorden();
    }
}
