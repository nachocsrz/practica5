/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 * Nodo.java
 * Clase que genera objetos de tipo Nodo, los cuales son elementos de un arbol
 * binario. 
 * @author Castillo Suarez Ignacio, Mendez Olivares Ana Itzel
 * @param <E> Tipo de dato generico del nodo
 * @version Oct 2019
 */
public class Nodo<E>{
    protected E elemento;
    protected int altura;
    protected Nodo<E> izquierdo;
    protected Nodo<E> derecho;
     /**
      * Constructor por parametros que recibe un objeto de tipo E y genara
      * un nodo para el arbol con el elemento recibido
      * @param elemento Elemento contenido en el nodo creado
      */
    public Nodo(E elemento){
        this.elemento = elemento;
    }
    
    /**
     * Constructor por parametros que, ademas de recibir el elemento del nodo,
     * recibe los hijos izquierdo y derecho del mismo nodo
     * @param izquierdo Hijo izquierdo del nodo creado
     * @param derecho Hijo izquierdo del nodo creado
     * @param elemento Elemento contenido en el nodo creado
     */
    public Nodo(Nodo<E> izquierdo, Nodo<E> derecho, E elemento){
        this.elemento = elemento;
        this.izquierdo = izquierdo;
        this.derecho = derecho;
    }
 
    /**
     * Setter del elemento dentro de un nodo
     * @param elemento Elemento a establecer en el nodo
     */
    public void setElemento(E elemento) {
        this.elemento = elemento;
    }

    /**
     * Setter del hijo izquierdo de un nodo
     * @param izquierdo Nodo a establecer como hijo izquierdo
     */
    public void setIzquierdo(Nodo<E> izquierdo) {
        this.izquierdo = izquierdo;
    }

    /**
     * Setter del hijo derecho de un nodo
     * @param derecho Nodo a establecer como hijo derecho
     */
    public void setDerecho(Nodo<E> derecho) {
        this.derecho = derecho;
    }

    /**
     * Getter del elemento contenido en un nodo
     * @return El elemento del nodo
     */
    public E getElemento() {
        return elemento;
    }

    /**
     * Getter del hijo izquierdo de un nodo
     * @return El hijo izquierdo del nodo
     */
    public Nodo<E> getIzquierdo() {
        return izquierdo;
    }

    /**
     * Getter del hijo derecho de un nodo
     * @return El hijo derecho del nodo
     */
    public Nodo<E> getDerecho() {
        return derecho;
    }

    /**
     * Getter de la altura del nodo, se apoya del metodo auxiliar calcularAltura
     * @return La altura del nodo
     */
    public int getAltura() {
        setAltura(calcularAltura());
        return altura;
    }

    /**
     * Setter de la altura del nodo
     * @param altura Altura a establecer del para el nodo
     */
    public void setAltura(int altura) {
        this.altura = altura;
    }
   
    /**
     * Metodo que crea un hijo derecho o izquierdo al nodo apoyandose de un 
     * dato booleano que determina si el hijo es izquierdo
     * @param esIzquierdo Booleano para establecer la posicion del hijo
     */
    public void crearHijo(boolean esIzquierdo) {
        Nodo aux = new Nodo(null);
        if (esIzquierdo) {
                this.izquierdo = aux;
        } else {
                this.derecho = aux;
        }
    }
    
    /**
     * Metodo que calcula la altura del nodo de manera recursiva; se apoya
     * en las alturas de los hijos del nodo
     * @return La altura relacionada al nodo
     */
    public int calcularAltura(){
        //Si el nodo no tiene hijo, la altura es 0
        if(izquierdo == null && derecho == null){
            return 0;
        } else{
            //Si no tiene hijo izquierdo, la altura es la del hijo derecho + 1
            if(izquierdo == null){
                return derecho.calcularAltura() + 1;
            } else 
            //Si no tiene hijo derecho, la altura es la del hijo izquierdo +1    
            if(derecho == null){
                return izquierdo.calcularAltura() + 1;
            }
            else
            //Si niente ambos hijo, la altura es max(alturaIzquierdo, alturaDerecho) +1    
            if(izquierdo.getAltura() > derecho.getAltura()){
               return izquierdo.calcularAltura() + 1;
                        } else {
                return derecho.calcularAltura() + 1;
            }
            
        }
    }

    /**
     * Metodo para la representacion del nodo al usuario
     * @return Cadena con la forma del nodo
     */
    @Override
    public String toString(){
      return getElemento().toString();  
    }
}
