/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package arboles;

/**
 * ArbolBinario.java
 * Clase que genera objetos de tipo Arbol Binario, cuyo objetivo es la
 * insercion y organizacion de datos. 
 * @author Castillo Suarez Ignacio, Mendez Olivares Ana Itzel
 * @param <E> Tipo de dato generico del Arbol
 * @version Oct 2019
 */
public class ArbolBinario<E> {
    protected Nodo<E> raiz;
    /**
     * Constructor por parametros que recibe un nodo y genera al arbol con 
     * respecto a este
     * @param n Nodo con el cual general el arbol
     */
    public ArbolBinario(Nodo<E> n){
        raiz = n;
    }

    /**
     * Getter de la raiz del arbol
     * @return Raiz del arbol
     */
    public Nodo<E> getRaiz() {
        return raiz;
    }

    /**
     * Setter de la raiz del arbol
     * @param raiz Nodo para establecer la raiz
     */
    public void setRaiz(Nodo<E> raiz) {
        this.raiz = raiz;
    }
    
    /**
     * Metodo auxiliar que lleva a cabo el recorrido en preorden del arbol,
     * partiendo del nodo ingresado, generalmente la raiz
     * @param raiz Nodo en donde empezar el recorrido
     */
    private void recorrerPreorden(Nodo<E> raiz){
        //De no existir el nodo, el metodo no regresa nada
        if(raiz == null)
            return;
        //Cuando el nodo existe, se visita y se prosigue primero con el hijo
        //izquierdo y luego el derecho
        visitar(raiz);
        recorrerPreorden(raiz.getIzquierdo());
        recorrerPreorden(raiz.getDerecho());      
    }
    
    /**
     * Metodo auxiliar que lleva a cabo el recorrido en postorden del arbol,
     * partiendo de un nodo ingresado, generalmente la raiz
     * @param raiz Nodo en donde empezar el recorrido
     */
    private void recorrerPostorden(Nodo<E> raiz){
        //De no existir el nodo, el metodo no regresa nada
        if(raiz == null)
            return;
        //Cuando el nodo existe, se recorre el hijo izquierdo, luego el derecho
        //y al final se visita
        recorrerPostorden(raiz.getIzquierdo());
        recorrerPostorden(raiz.getDerecho());
        visitar(raiz);
    }
    
   /**
     * Metodo auxiliar que lleva a cabo el recorrido en inorden del arbol,
     * partiendo de un nodo ingresado, generalmente la raiz
     * @param raiz Nodo en donde empezar el recorrido
     */
    private void recorrerInorden(Nodo<E> raiz){
        //De no existir el nodo, el metodo no regresa nada
        if(raiz == null)
            return;
        //Cuando el nodo existe, se recorre el hijo izquierdo, se visita
        //y al final se recorre el hijo derecho
        recorrerInorden(raiz.getIzquierdo());
         visitar(raiz);
        recorrerInorden(raiz.getDerecho());
       
    }
    /**
     * Metodo que lleva a cabo el recorrido en preorden.
     */
    public void recorrerPreorden(){
        recorrerPreorden(raiz);
    }
    
    /**
     * Metodo que lleva a cabo el recorrido en postorden.
     */
    public void recorrerPostorden(){
        recorrerPostorden(raiz);
    }
    /**
     * Metodo que lleva a cabo el recorrido en inorden.
     */
    public void recorrerInorden(){
        recorrerInorden(raiz);
    }
    
    /**
     * Metodo auxiliar que imprime el nodo a visitar
     * @param n El nodo a visitar
     */
    public void visitar(Nodo n){
        System.out.println(n.getElemento());
    }
    /**
     * Metodo para la representacion del arbol binario al usuario; se apoya
     * en el metodo auxiliar crearRepresentacion
     * @return Cadena con la forma del arbol binario
     */
    @Override
    public String toString() {
        return crearRepresentacion(raiz, "", "", false);
    }

    /**
     * Metodo auxiliar qie genera la representacion del arbol a manera de 
     * cadena de caracteres
     * @param nodo Nodo del cual se parte para la representacion
     * @param representacion Cadena inicial para la representacion
     * @param nivel Cadena que indica el nivel del arbol
     * @param esIzquierdo Booleano que indica si el nodo ingresado es hijo
     * izquierdo
     * @return La representacion en cadena del arbol binario
     */
    private String crearRepresentacion(Nodo<E> nodo, String representacion, 
            String nivel, boolean esIzquierdo) {
        
        representacion += nivel;
                
        if (!nivel.equals("")) {
            representacion += "\b\b"+ 
                    (esIzquierdo ? "\u251C": "\u2514") + "\u2500";
        }
        
        if(nodo == null)
            return representacion += "\n";
        
        representacion += " " + nodo + "\n";
        
        // Hijo izquierdo
        representacion = crearRepresentacion(nodo.getIzquierdo(), 
                representacion, nivel + "\u2502 ", true);
        // Hijo derecho
        representacion = crearRepresentacion(nodo.getDerecho(),
                representacion, nivel + "  ", false);
        
        return representacion;
    }
}
