package arboles;

/**
 * ArbolBinarioBusqueda.java
 * Clase que genera arboles binarios de busqueda, en donde se implementan los
 * metodos para añadir elementos al arbol
 * @author Castillo Suarez Ignacio, Mendez Olivares Ana Itzel
 * @param <E> Tipo de dato generico del arbol binario de busqueda
 * @version Oct 2019
 */
public class ArbolBinarioBusqueda<E extends Comparable<E>> extends ArbolBinario {

    /**
     * Constructor por parametros que recibe un objeto de tipo Nodo para
     * el arbol.
     * @param n Nodo en el arbol
     */
    public ArbolBinarioBusqueda(Nodo<E> n) {
        super(n);
    }

    /**
     * Metodo auxiliar que inserta un elemento a un nodo dado,
     * si el nodo tiene un elemento, el elemento se inserta en alguno de sus hijos
     * @param elemento Elemento contenido en el nodo a insertar
     * @param nodo Nodo con respecto al cual se va a insertar
     */
    public void insertar(E elemento, Nodo<E> nodo) {
        //Si el nodo no tiene elemento, le asignamos elemento
        if (nodo.getElemento() == null) {
            nodo.setElemento(elemento);
        } else {
            //Si el nodo tiene elemento, lo comparamos con elemento
            int comparacion = elemento.compareTo(nodo.getElemento());
            switch (comparacion) {
                //Si los elementos son iguales, no insertamos nada 
                case 0:
                    break;
                //Si elemento es menor que el elemento del nodo
                //Insertamos el elemento en el hijo izquierdo    
                case -1:
                    //Si el nodo no tiene hijo izquierdo creamos uno
                    if (nodo.getIzquierdo() == null) {
                        nodo.crearHijo(true);
                    }
                    insertar(elemento, nodo.getIzquierdo());
                    break;
                //Si elemento es mayor que el elemento del nodo
                //Insertamos el elemento en el hijo derecho
                case 1:
                    //Si el nodo no tiene hijo derecho, creamos uno
                    if (nodo.getDerecho() == null) {
                        nodo.crearHijo(false);
                    }
                    insertar(elemento, nodo.getDerecho());
                    break;
            }
        }
    }

    /**
     * Metodo que inserta un elemento a este arbol haciendo uso del metodo
     * auxiliar insertar.
     * @param elemento
     */
    public void insertar(E elemento) {
        //Se inserta el nuevo nodo con respecto a la raiz del arbol
        insertar(elemento, raiz);
    }
}
